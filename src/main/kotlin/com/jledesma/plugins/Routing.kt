package com.jledesma.plugins

import com.jledesma.root.AllNote
import com.jledesma.root.AllTodo
import com.jledesma.root.root
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*

fun Application.configureRouting() {
    routing {
        //get("/") {
        //    call.respondText("Hello World!")
        //}
        root()
        AllTodo()
        AllNote()
    }
}
