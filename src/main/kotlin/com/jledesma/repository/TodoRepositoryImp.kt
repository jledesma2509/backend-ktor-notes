package com.jledesma.repository

import com.jledesma.database.DatabaseMySQLManager
import com.jledesma.models.ToDo

class TodoRepositoryImp : TodoRepository {

    private val database = DatabaseMySQLManager()

    override fun getAllTodos(): List<ToDo> {
        return database.getAllTodos().map {
            ToDo(id=it.id, title = it.title,done = it.done)
        }
    }

    override fun getTodo(id: Int): ToDo? {
        return database.getTodo(id)?.let {
            ToDo(id = it.id,title = it.title,done = it.done)
        }
    }

    @Throws(Exception::class)
    override fun addTodo(draft: ToDo): ToDo  {
        return database.addTodo(draft)
    }

    override fun removeTodo(id: Int): Boolean {
        return database.removeTodo(id)
    }

    @Throws(Exception::class)
    override fun updateTodo(id: Int, draft: ToDo): Boolean {
        return database.updateTodo(id,draft)
    }

}