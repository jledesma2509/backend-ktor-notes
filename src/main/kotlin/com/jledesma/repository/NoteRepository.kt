package com.jledesma.repository

import com.jledesma.models.Note

interface NoteRepository {

    fun getAllNotes():List<Note>
    fun getNote(id:Int):Note?
    fun addNote(draft:Note):Note
    fun removeNote(id:Int):Boolean
    fun updateNote(id:Int,draft:Note):Boolean

}