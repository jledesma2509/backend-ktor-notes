package com.jledesma.repository

import com.jledesma.database.DatabaseMySQLManager
import com.jledesma.database.DatabaseSQLManager
import com.jledesma.models.Note
import com.jledesma.models.ToDo

class NoteRepositoryImp : NoteRepository {

    private val database = DatabaseSQLManager()

    override fun getAllNotes(): List<Note> {
        return database.getAllNotes().map {
            Note(id=it.id, title = it.title,description = it.description)
        }
    }

    override fun getNote(id: Int): Note? {
        return database.getNote(id)?.let {
            Note(id = it.id,title = it.title,description = it.description)
        }
    }

    @Throws(Exception::class)
    override fun addNote(draft: Note): Note  {
        return database.addNote(draft)
    }

    override fun removeNote(id: Int): Boolean {
        return database.removeNote(id)
    }

    @Throws(Exception::class)
    override fun updateNote(id: Int, draft: Note): Boolean {
        return database.updateNote(id,draft)
    }

}