package com.jledesma.repository

import com.jledesma.models.ToDo

interface TodoRepository {

    fun getAllTodos():List<ToDo>
    fun getTodo(id:Int):ToDo?
    fun addTodo(draft:ToDo):ToDo
    fun removeTodo(id:Int):Boolean
    fun updateTodo(id:Int,draft:ToDo):Boolean

}