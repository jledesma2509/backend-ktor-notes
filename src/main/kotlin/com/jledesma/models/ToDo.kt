package com.jledesma.models

import kotlinx.serialization.Serializable

@Serializable
data class ToDo(
    val id:Int?=0,
    val title:String,
    val done:Boolean
)