package com.jledesma.root

import com.jledesma.models.ToDo
import com.jledesma.repository.TodoRepository
import com.jledesma.repository.TodoRepositoryImp
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.AllTodo(){

    //Repositorio para conexion a MYSQL
    val repository : TodoRepository = TodoRepositoryImp()

    get("/") {
        call.respond(message = "Welcome my Api with Ktor",status = HttpStatusCode.OK)
    }

    get("/todos") {
        val todos = repository.getAllTodos()
        call.respond(message = todos,status = HttpStatusCode.OK)
    }
    get("/todos/{id}") {
        val id = call.parameters["id"]?.toIntOrNull()
        if(id == null){
            call.respond(message = "El parametro id debe ser un numero", status = HttpStatusCode.BadRequest)
            return@get
        }
        val todo = repository.getTodo(id)
        todo?.let {
            call.respond(message = it, status = HttpStatusCode.OK)
        }?: kotlin.run {
            call.respond(message = "No pudimos encontra la nota con id = $id", status = HttpStatusCode.NotFound)
        }
    }
    post("/todos") {
        try{
            val todoDraft = call.receive<ToDo>()
            val todo = repository.addTodo(todoDraft)
            call.respond(message = todo, status = HttpStatusCode.OK)
        }catch (ex:Exception){
            call.respond(message = "${ex.message}", status = HttpStatusCode.BadRequest)
        }

    }
    put("/todos/{id}") {
        try {
            val todoDraft = call.receive<ToDo>()
            val id = call.parameters["id"]?.toIntOrNull()
            if (id == null) {
                call.respond(message = "El parametro id debe ser un numero", status = HttpStatusCode.BadRequest)
                return@put
            }
            val updated = repository.updateTodo(id, todoDraft)
            if (updated) {
                call.respond(message = "Nota actualizada", status = HttpStatusCode.OK)
            } else {
                call.respond(message = "No pudimos actualizar la nota con id = $id", status = HttpStatusCode.NotFound)
            }
        }catch (ex:Exception){
            call.respond(message = "${ex.message}", status = HttpStatusCode.BadRequest)
        }
    }
    delete("/todos/{id}") {
        val id = call.parameters["id"]?.toIntOrNull()
        if(id == null){
            call.respond(message = "El parametro id debe ser un numero", status = HttpStatusCode.BadRequest)
            return@delete
        }
        val deleted = repository.removeTodo(id)
        if(deleted){
            call.respond(message = "Nota eliminada", status = HttpStatusCode.OK)
        }else{
            call.respond(message = "No pudimos eliminar la nota con id = $id", status = HttpStatusCode.NotFound)
        }
    }
}