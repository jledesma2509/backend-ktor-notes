package com.jledesma.root

import com.jledesma.models.Note
import com.jledesma.models.ToDo
import com.jledesma.repository.NoteRepository
import com.jledesma.repository.NoteRepositoryImp
import com.jledesma.repository.TodoRepository
import com.jledesma.repository.TodoRepositoryImp
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.AllNote(){

    //Repositorio para conexion a MYSQL
    val repository : NoteRepository = NoteRepositoryImp()

    get("/") {
        call.respond(message = "Welcome my Api with Ktor",status = HttpStatusCode.OK)
    }

    get("/notes") {
        val todos = repository.getAllNotes()
        call.respond(message = todos,status = HttpStatusCode.OK)
    }
    get("/notes/{id}") {
        val id = call.parameters["id"]?.toIntOrNull()
        if(id == null){
            call.respond(message = "El parametro id debe ser un numero", status = HttpStatusCode.BadRequest)
            return@get
        }
        val todo = repository.getNote(id)
        todo?.let {
            call.respond(message = it, status = HttpStatusCode.OK)
        }?: kotlin.run {
            call.respond(message = "No pudimos encontra la nota con id = $id", status = HttpStatusCode.NotFound)
        }
    }
    post("/notes") {
        try{
            val todoDraft = call.receive<Note>()
            val todo = repository.addNote(todoDraft)
            call.respond(message = todo, status = HttpStatusCode.OK)
        }catch (ex:Exception){
            call.respond(message = "${ex.message}", status = HttpStatusCode.BadRequest)
        }

    }
    put("/notes/{id}") {
        try {
            val todoDraft = call.receive<Note>()
            val id = call.parameters["id"]?.toIntOrNull()
            if (id == null) {
                call.respond(message = "El parametro id debe ser un numero", status = HttpStatusCode.BadRequest)
                return@put
            }
            val updated = repository.updateNote(id, todoDraft)
            if (updated) {
                call.respond(message = "Nota actualizada", status = HttpStatusCode.OK)
            } else {
                call.respond(message = "No pudimos actualizar la nota con id = $id", status = HttpStatusCode.NotFound)
            }
        }catch (ex:Exception){
            call.respond(message = "${ex.message}", status = HttpStatusCode.BadRequest)
        }
    }
    delete("/notes/{id}") {
        val id = call.parameters["id"]?.toIntOrNull()
        if(id == null){
            call.respond(message = "El parametro id debe ser un numero", status = HttpStatusCode.BadRequest)
            return@delete
        }
        val deleted = repository.removeNote(id)
        if(deleted){
            call.respond(message = "Nota eliminada", status = HttpStatusCode.OK)
        }else{
            call.respond(message = "No pudimos eliminar la nota con id = $id", status = HttpStatusCode.NotFound)
        }
    }
}