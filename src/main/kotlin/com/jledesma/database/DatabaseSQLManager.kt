package com.jledesma.database

import com.jledesma.models.Note
import com.jledesma.models.ToDo
import org.ktorm.database.Database
import org.ktorm.dsl.*
import org.ktorm.entity.*

class DatabaseSQLManager {

    private val hostName = "LT230301\\SQLEXPRESS"
    private val databaseName = "db_todo"
    private val userName = "uSQL"
    private val password = "SQL2023"

    private val ktormDatabase : Database

    init {
        val jdbcUrl = "jdbc:sqlserver://$hostName;databaseName=$databaseName;user=$userName;password=$password"
        ktormDatabase = Database.connect(jdbcUrl)
    }



    fun getAllNotes():List<DBNoteEntity>{
        return ktormDatabase.sequenceOf(DBNoteTable).toList()
    }

    fun getNote(id:Int):Note?{
        //return ktormDatabase.sequenceOf(DBNoteTable)  { it.id eq id }
        val query = ktormDatabase.from(DBNoteTable).select(
            DBNoteTable.id,
            DBNoteTable.title,
            DBNoteTable.description
        ).where {
            (DBNoteTable.id eq id)
        }

        return query
            .map { row -> Note(row[DBNoteTable.id], row[DBNoteTable.title]!!, row[DBNoteTable.description]!!) }.first()
    }
    fun addNote(draft:Note):Note{
        try{
            val insertId = ktormDatabase.insertAndGenerateKey(DBNoteTable){
                set(DBNoteTable.title, draft.title)
                set(DBNoteTable.description, draft.description)
            } as Int
            return Note(insertId,draft.title,draft.description)
        }catch (ex:Exception){
            throw ex
        }

    }
    fun updateNote(id:Int,draft:Note):Boolean{
        try {
            val updateRows = ktormDatabase.update(DBNoteTable) {
                set(DBNoteTable.title, draft.title)
                set(DBNoteTable.description, draft.description)
                where {
                    it.id eq id
                }
            } as Int
            return updateRows > 0
        }catch (ex:Exception){
            throw ex
        }
    }
    fun removeNote(id:Int):Boolean{
        val deleteRows = ktormDatabase.delete(DBNoteTable){
            it.id eq id
        } as Int
        return deleteRows > 0
    }
}