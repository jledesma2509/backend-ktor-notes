package com.jledesma.database

import com.jledesma.models.ToDo
import org.ktorm.database.Database
import org.ktorm.dsl.*
import org.ktorm.entity.firstOrNull
import org.ktorm.entity.sequenceOf
import org.ktorm.entity.toList

class DatabaseMySQLManager {

    private val hostName = "localhost"
    private val databaseName = "db_todo"
    private val userName = "root" //user_todo
    private val password = "12345678" //Mysql@2022

    private val ktormDatabase : Database

    init {
        val jdbcUrl = "jdbc:mysql://$hostName:3306/$databaseName?user=$userName&password=$password&useSSL=false&serverTimezone=UTC"
        ktormDatabase = Database.connect(jdbcUrl)
    }


    fun getAllTodos():List<DBTodoEntity>{
        return ktormDatabase.sequenceOf(DBTodoTable).toList()
    }

    fun getTodo(id:Int):DBTodoEntity?{
        return ktormDatabase.sequenceOf(DBTodoTable).firstOrNull { it.id eq id }
    }
    fun addTodo(draft:ToDo):ToDo{
        try{
            val insertId = ktormDatabase.insertAndGenerateKey(DBTodoTable){
                set(DBTodoTable.title, draft.title)
                set(DBTodoTable.done, draft.done)
            } as Int
            return ToDo(insertId,draft.title,draft.done)
        }catch (ex:Exception){
            throw ex
        }

    }
    fun updateTodo(id:Int,draft:ToDo):Boolean{
        try {
            val updateRows = ktormDatabase.update(DBTodoTable) {
                set(DBTodoTable.title, draft.title)
                set(DBTodoTable.done, draft.done)
                where {
                    it.id eq id
                }
            } as Int
            return updateRows > 0
        }catch (ex:Exception){
            throw ex
        }
    }
    fun removeTodo(id:Int):Boolean{
        val deleteRows = ktormDatabase.delete(DBTodoTable){
            it.id eq id
        } as Int
        return deleteRows > 0
    }
}