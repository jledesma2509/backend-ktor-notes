package com.jledesma.database

import org.ktorm.entity.Entity
import org.ktorm.schema.Table
import org.ktorm.schema.boolean
import org.ktorm.schema.int
import org.ktorm.schema.varchar


object DBTodoTable:Table<DBTodoEntity>("todo"){

    val id = int("id").primaryKey().bindTo { it.id}
    val title = varchar("title").bindTo { it.title }
    val done = boolean("done").bindTo { it.done }

}

interface DBTodoEntity : Entity<DBTodoEntity>{

    companion object : Entity.Factory<DBTodoEntity>()
    val id:Int
    val title:String
    val done:Boolean
}

object DBNoteTable:Table<DBNoteEntity>("note"){

    val id = int("id").primaryKey().bindTo { it.id}
    val title = varchar("title").bindTo { it.title }
    val description = varchar("description").bindTo { it.description }

}

interface DBNoteEntity : Entity<DBNoteEntity>{

    companion object : Entity.Factory<DBNoteEntity>()
    val id:Int
    val title:String
    val description:String
}